//
//  AppDelegate.h
//  LibPigtailSampleApp
//
//  Created by Nick Garner on 9/12/14.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
