//
//  ComponentsSeparatedByChar.m
//  RawRead
//
//  Created by Jonathan Cormier on 11-11-29.
//  Copyright 2011 Geek Theory Inc. All rights reserved.
//

#import "ComponentsSeparatedByByte.h"


@implementation NSData(ComponentsSeparatedByByte)

- (NSArray *)componentsSeparatedByByte:(Byte)sep;
{
	int len, index, last_sep_index;
	NSData *line;
	NSMutableArray *lines = nil;
	
	len = [self length];
	Byte cData[len];
	
	[self getBytes:cData length:len];
	
	index = last_sep_index = 0;
	
	lines = [[NSMutableArray alloc] init];
	
	do {
		if (sep == cData[index])
		{
			NSRange startEndRange = NSMakeRange(last_sep_index, index - last_sep_index);
			line = [self subdataWithRange:startEndRange];
			
			[lines addObject:line];
			
			last_sep_index = index + 1;
			
			continue;
		}
	} while (index++ < len);
	
	return lines;
}

@end
