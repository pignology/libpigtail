//
//  ComponentsSeparatedByByte.h
//  RawRead
//
//  Created by Jonathan Cormier on 11-11-29.
//  Copyright 2011 Geek Theory Inc. All rights reserved.
//



@interface NSData (ComponentsSeparatedByByte)
	- (NSArray *)componentsSeparatedByByte:(Byte)sep;
@end
