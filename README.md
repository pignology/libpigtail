libpigtail provides a basic Pigtail class that can be used in your iOS app to discover, connect to, and query a Pigtail or Piglet from Pignology, LLC.

A sample application is included that demonstrates discovery, connecting and updating a label with the current freq and mode of the connected radio.

The following radio manufacturers are supported.

* Elecraft  
* Old Yaesu (FT8X7)  
* New Yaesu  
* Icom  
* Kenwood  

***
Can't find the answer to your question in any of the [wiki](https://bitbucket.org/pignology/libpigtail/wiki/Home) articles? You can email support@pignology.net.